#Events Calendar App

This is an app built with Angular 2+:

##Installation:

1. clone the repository
```git clone git@bitbucket.org:yaroslav_grishajev/calendar-app.git```

2. install modules
```npm i```

3. start development server
```npm start```

4. or build project dist
```NODE_ENV=production npm run build```

APP is available on http://localhost:8080/ by default.

Events are marked with colored border. Conflicted events are colored red.
Also there is a feature to filter events by user.
