import { Component } from '@angular/core'

import template from './events-app.html'

@Component({
  template,
  selector: 'events-app',
})
export class EventsApp {}
