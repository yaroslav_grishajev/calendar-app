import { Component } from '@angular/core'
import { Inject } from 'lib'

import { Events } from 'app/services/events'

import template from './events-calendar.html'
import './events-calendar.scss'

const TIME = {
  weekDays: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
  hours: generateHourTitles(),
  weekTime: 7 * 24 * 60 * 60 * 1000,
  dayTime: 24 * 60 * 60 * 1000,
}

const COLORS = ['green', 'purple', 'red', 'yellow']

@Inject(Events)
@Component({
  template,
  selector: 'events-calendar',
})
export class EventsCalendar {
  get mondayWithOffset() {
    return this.monday.getTime() + this.offset * TIME.weekTime
  }

  get weekStartDay() {
    const weekStartDate = new Date(this.mondayWithOffset)
    const start = new Date(weekStartDate.getFullYear(), 0, 0)
    const diff = weekStartDate - start

    return Math.floor(diff / TIME.dayTime)
  }

  get weekEndDay() {
    return this.weekStartDay + 6
  }

  constructor(events) {
    this.events = events
    this.monday = this.getCurrentMonday()
    this.offset = 0
    this.days = []
    this.colors = {}
    this.users = new Set()
    this.hours = TIME.hours
    this.selectedUser = null
  }

  ngOnInit() {
    this.updateCalendar()
  }

  getCurrentMonday() {
    const now = new Date()
    const day = now.getDay()
    const mondayDay = now.getDate() - day + (day === 0 ? -6 : 1)

    return new Date(now.setDate(mondayDay))
  }

  updateCalendar() {
    return this.fetchEvents()
      .then(events => {
        this.generateUsers(events)
        this.generateDays(events)
        this.assignColors()
      })
  }

  generateUsers(events) {
    events.forEach(event => this.users.add(event.name))
  }

  fetchEvents() {
    return this.events.get({
      user: this.selectedUser,
      start: this.weekStartDay,
      end: this.weekEndDay,
      year: new Date(this.mondayWithOffset).getFullYear().toString(),
    })
  }

  generateDays(weekEvents) {
    this.days = TIME.weekDays.map((day, index) => {
      const date = new Date(this.mondayWithOffset + index * TIME.dayTime)
      const title = `${day} ${date.getDate()}.${date.getMonth() + 1}`
      const events = this.getEventsByDay(weekEvents, index)
      const groups = this.groupOverlaps(events)

      this.markConflictsInGroups(groups)

      return { title, groups }
    })
  }

  assignColors() {
    let index = 0

    this.users.forEach(user => {
      index = COLORS[index] ? index : 0

      this.colors[user] = COLORS[index]
      index++
    })
  }

  getEventsByDay(weekEvents, weekDay) {
    return weekEvents.filter(event => event.day === this.weekStartDay + weekDay)
  }

  groupOverlaps(events) {
    if (events.length < 2) {
      return this.createGroupFrom(events[0])
    }

    return events
      .sort((left, right) => left.start > right.start)
      .reduce((overlaps, event) => {
        if (!overlaps.length) {
          return overlaps.concat(this.createGroupFrom(event))
        }

        const overlapGroup = overlaps.find(overlap => this.overlaps(overlap.meta, event))

        if (!overlapGroup) {
          return overlaps.concat(this.createGroupFrom(event))
        }

        this.updateGroupWith(overlapGroup, event)

        return overlaps
      }, [])
  }

  createGroupFrom(event) {
    if (!event) {
      return []
    }

    return [{
      meta: {
        start: event.start,
        end: event.end,
      },
      events: [event],
    }]
  }

  updateGroupWith(group, event) {
    if (group.meta.start > event.start) {
      group.meta.start = event.start
    }

    if (group.meta.end < event.end) {
      group.meta.end = event.end
    }

    group.events.push(event)
  }

  overlaps(left, right) {
    return right.start >= left.start && right.start < left.end ||
        right.start < left.start && left.start < right.end ||
        right.start <= left.start && right.end >= left.end ||
        left.start < right.start && left.end > right.end
  }

  markConflictsInGroups(groups) {
    groups.forEach(group => {
      if (this.isComparable(group.events)) {
        this.markConflicts(group.events)
      }
    })
  }

  markConflicts(events) {
    const groupedEvents = this.groupEventsByName(events)

    Object.keys(groupedEvents).forEach(name => {
      if (this.isComparable(groupedEvents[name])) {
        this.groupOverlaps(groupedEvents[name]).forEach(overlapGroup => {
          if (this.isComparable(overlapGroup.events)) {
            overlapGroup.events.forEach(event => event.isConflicted = true)
          }
        })
      }
    })
  }

  groupEventsByName(events) {
    return events.reduce((acc, event) => {
      if (acc[event.name]) {
        acc[event.name].push(event)
      } else {
        acc[event.name] = [event]
      }

      return acc
    }, {})
  }

  isComparable(array) {
    return array.length > 1
  }

  changeWeek(offset) {
    this.offset += offset

    this.updateCalendar()
  }

  positionTop(event) {
    const halfAnHour = 30
    const cellHeight = 30

    const cellFromTop = event.start / halfAnHour

    return cellFromTop * cellHeight
  }

  positionBottom(event) {
    const halfAnHour = 30
    const cellHeight = 30
    const totalCells = 25

    const cellFromTop = event.end / halfAnHour
    const cellFromBottom = totalCells - cellFromTop

    return cellFromBottom > 0 ? cellFromBottom * cellHeight : 0
  }
}

function generateHourTitles() {
  const hours = []
  const start = 9
  const end = 21

  for (let i = start; i <= end; i++) {
    if (i !== end) {
      hours.push(`${i}:00`, `${i}:30`)

      continue
    }

    hours.push(`${i}:00`)
  }

  return hours
}
