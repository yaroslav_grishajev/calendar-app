import { enableProdMode } from '@angular/core'
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'
import { BootModule } from './module'

if (IS_PRODUCTION) {
  enableProdMode()
} else {
  Error.stackTraceLimit = Infinity
}

platformBrowserDynamic().bootstrapModule(BootModule)
