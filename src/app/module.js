import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { CommonModule, APP_BASE_HREF } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { BrowserModule } from '@angular/platform-browser'
import { HttpModule } from '@angular/http'

import { EventsApp } from 'app/components/events-app/events-app'
import { EventsCalendar } from 'app/components/events-calendar/events-calendar'

import { Events } from './services/events'

import 'styles/app.scss'

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule,
    HttpModule,
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    Events,
  ],
  declarations: [
    EventsApp,
    EventsCalendar,
  ],
  entryComponents: [
  ],
  bootstrap: [
    EventsApp,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class BootModule {}
