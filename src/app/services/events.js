import { Http } from '@angular/http'
import { Inject } from 'lib'
import 'rxjs/add/operator/toPromise'

@Inject(Http)
export class Events {
  constructor(http) {
    this.http = http
  }

  get(params) {
    return this.fetchAll()
      .then(personEvents => this.getAssignedEventsByDates(personEvents, params))
  }

  getAssignedEventsByDates(personEvents, params) {
    return this.filterByUser(personEvents, params.user)
      .reduce((accumulate, person) => {
        const filteredAndAssignedEvents = person.events.filter(event => {
          return this.checkDatesAndAssign(event, params, person)
        })

        return [...accumulate, ...filteredAndAssignedEvents]
      }, [])
  }

  filterByUser(personEvents, user) {
    return user ? personEvents.filter(person => person.name === user) : personEvents
  }

  checkDatesAndAssign(event, params, person) {
    const isBetweenDates = event.year === params.year &&
    event.day >= params.start &&
    event.day <= params.end

    return isBetweenDates && Object.assign(event, { name: person.name })
  }

  fetchAll() {
    return this.http.get('src/app/services/events.json')
      .toPromise()
      .then(response => response.json())
  }
}
