import {
  Inject as InjectMetadata,
  OpaqueToken,
} from '@angular/core'

export function Inject(...dependencies) {
  return function configureClassDependencies(cls) {
    const type = cls
    const parentParameters = type.parameters || []

    type.parameters = dependencies.map(typeOrArray => {
      let dependency = typeOrArray

      if (!Array.isArray(typeOrArray)) {
        dependency = [typeOrArray]
      }

      if (dependency[dependency.length - 1] instanceof OpaqueToken) {
        dependency[dependency.length - 1] = new InjectMetadata(dependency[dependency.length - 1])
      }

      return dependency
    }).concat(parentParameters)
  }
}
