const path = require('path')
const babel = require('babel-core')
const os = require('os')
const fs = require('fs')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const autoprefixer = require('autoprefixer')
const packageInfo = require('./package.json')

const ENV = packageInfo.config.env = process.env.NODE_ENV || process.env.ENV || 'development'
const isProduction = ENV === 'production'
const isDevServer = !!process.argv.find(v => v.includes('webpack-dev-server'))
const canMinify = isProduction && process.argv.indexOf('--no-min') === -1
const envSpecificPlugins = []
const resolve = (filePath) => path.join(__dirname, filePath)
let loadStyles = (notExtractLoader, loader) => `${notExtractLoader}!${loader}`

if (isProduction) {
  loadStyles = ExtractTextPlugin.extract

  envSpecificPlugins.push(
    new ExtractTextPlugin('styles.[chunkhash].css'),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.DedupePlugin()
  )
} else {
  envSpecificPlugins.push(
    new webpack.NoErrorsPlugin()
  )
}

if (canMinify) {
  envSpecificPlugins.push(
    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      comments: false,
      sourceMap: true,
      mangle: {
        screw_ie8: true,
        keep_fnames: true,
      },
      compress: {
        screw_ie8: true,
        warnings: false,
      },
    })
  )
}

module.exports = {
  entry: {
    app: [
      './src/app/index.js',
    ],
    vendor: [
      'core-js/client/shim.min',
      'reflect-metadata',
      'zone.js',
      'bootstrap-sass',
      externalBabelHelpers(),
    ],
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: filename('[name].js', '[chunkhash]'),
    chunkFilename: filename('[name].chunk.js', '[chunkhash]'),
    publicPath: '/',
  },
  devtool: isProduction ? '#source-map' : '#cheap-source-map',
  resolve: {
    extensions: ['', '.js', '.scss'],
    root: [
      resolve('src'),
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      IS_PRODUCTION: isProduction,
      APP_CONFIG: JSON.stringify(buildEnvVars()),
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
    new HtmlWebpackPlugin({
      template: path.resolve('./src/index.html'),
      inject: 'body',
    }),
  ].concat(envSpecificPlugins),
  module: {
    noParse: /\.min\.js$/,
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: [resolve('src'), resolve('tools')],
        query: {
          presets: ['es2015', 'stage-1'],
          plugins: ['transform-decorators-legacy', 'external-helpers'],
        },
      },
      { test: /\.(png|jpe?g|gif)$/, loader: 'url-loader?limit=8192&name=img/[name].[ext]' },
      { test: /\.svg$/, loader: 'file?name=img/[name].[ext]' },
      {
        test: /\.(scss|css)$/,
        loader: loadStyles('style', 'css!postcss-loader!resolve-url!sass?sourceMap'),
      },
      { test: /\.html$/, loader: 'html' },
      {
        test: [/\.(eot|ttf|woff|woff2)/, /font-awesome\/fonts\//],
        loader: 'file?name=fonts/[name].[ext]',
      },
    ],
  },
  sassLoader: {
    includePaths: [
      resolve('node_modules'),
      resolve('src'),
      resolve('src/styles'),
    ],
  },
  postcss: [autoprefixer],
  htmlLoader: {
    root: resolve('src'),
    minimize: isProduction,
    removeAttributeQuotes: false,
    caseSensitive: true,
    customAttrSurround: [
      [/#/, /(?:)/],
      [/\*/, /(?:)/],
      [/\[?\(?/, /(?:)/],
    ],
    customAttrAssign: [/\)?]?=/],
  },
}

function filename(name, suffix = '[hash]') {
  return isProduction && !isDevServer ? name.replace(/\.([a-z]+)$/, `.${suffix}.$1`) : name
}

function externalBabelHelpers() {
  const filePath = `${os.tmpdir()}/babel-helpers.js`

  fs.writeFileSync(filePath, babel.buildExternalHelpers())

  return filePath
}

function buildEnvVars() {
  return Object.keys(packageInfo.config).reduce((config, name) => {
    config[name] = name in process.env ? process.env[name] : packageInfo.config[name]

    return config
  }, {})
}
